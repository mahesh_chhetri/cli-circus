<?php
include('settings.php');

// Creating connection
$conn = new mysqli(
    $GLOBALS['servername'],
    $GLOBALS['username'],
    $GLOBALS['password'],
    $GLOBALS['dbname']
);
// Creating table if not exists
$sql ="
    CREATE TABLE IF NOT EXISTS contact (
    user_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50),
    email VARCHAR(50),
    phone VARCHAR(50) NOT NULL,
    website VARCHAR(50) NOT NULL,
    message LONGTEXT,
    budget VARCHAR(50) NOT NULL,
    message_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
)";
$conn->query($sql);

// Storing to the database
$name = $_POST['name'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$website = $_POST['website'];
$message = $_POST['message'];
$budget = $_POST['budget'];


$timeStamp = new DateTime();
$timeStamp = $timeStamp->getTimestamp();
// sql query
$query = "INSERT INTO `contact` 
        (`user_id`, `name`, `email`,`phone`,`website`,`message`,`budget`,`message_date`) 
        VALUES (NULL,'".$name."', '".$email."','".$phone."','".$website."','".$message."','".$budget."', current_timestamp())";

$inserting = mysqli_query( $conn , $query);  
// Checking status and returning message
if($inserting){
    echo json_encode(array("status" => 'success',"error" => !$inserting));
    send_email($name,$email,$phone,$website,$message,$budget);
}else{
    echo json_encode(array("status" => 'fail',"error"=> !$inserting));
}


function send_email($name,$email,$phone,$website,$message,$budget){
    $formcontent=" From: $name \n Email: $email \n Phone: $phone \n Services: $website \n Message: $message \n Budget: $budget";
    $recipient = $GLOBALS['ownerEmail'];
    $subject = "Contact Form";
    $mailheader = "From: $email \r\n";
    // mail($recipient, $subject, $formcontent, $mailheader) or die("Error!");
    mail($recipient, $subject, $formcontent, $mailheader);
    die();
}


$conn->close();

?>
