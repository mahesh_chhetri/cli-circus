<?php
include('settings.php');

// Creating connection
$conn = new mysqli(
    $GLOBALS['servername'],
    $GLOBALS['username'],
    $GLOBALS['password'],
    $GLOBALS['dbname']
);
// Creating table if not exists
$sql ="
    CREATE TABLE IF NOT EXISTS subscribers (
    subscriber_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(50) NOT NULL UNIQUE,
    status boolean DEFAULT 1,
    reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
)";
$conn->query($sql);

// Storing to the database
$email = $_POST['email'];
$timeStamp = new DateTime();
$timeStamp = $timeStamp->getTimestamp();
// sql query
$query = "INSERT INTO `subscribers` (`subscriber_id`, `email`, `reg_date`) VALUES (NULL, '".$email."', current_timestamp())";
$inserting = mysqli_query( $conn , $query);  
// Checking status and returning message
if($inserting){
    echo json_encode(array("status" => 'success',"error" => !$inserting));
    emailToOwner($email);
}else{
    echo json_encode(array("status" => 'fail',"error"=> !$inserting));
}


function emailToOwner($email){
    $formcontent="Email: $email";
    $recipient = $GLOBALS['ownerEmail'];
    $subject = "Newsletter signup";
    $mailheader = "From: $email \r\n";
    mail($recipient, $subject, $formcontent, $mailheader) or die("Error!");
    mail($email, $subject, $formcontent, $mailheader) or die("Error!");
    // header("Location: http://makeyourweb.co.uk/redirect.html");
    // die();
}


$conn->close();


?>