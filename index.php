<?php include_once 'partials/header.php'?>
<?php include_once 'partials/slider.php'?>
<!-- About Section -->
<!-- <section class="about-section spad">
		<div class="container">
			<div class="section-title text-center">
				<h2>Welcome to Future Vision Agencey</h2>
				<p>Go wherever you heart takes you. Society will keep you in  their circus forever</p>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="about-img">
						<img src="assets/img/circus/3.jpg" alt="">
					</div>
				</div>
				<div class="col-lg-6">
					<div class="about-item">
						<div class="ai-text">
							<h4>Moto of our circus</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
						</div>
					</div>
					<div class="about-item">
						<div class="ai-text">
							<h4>Our expert area</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
						</div>
					</div>
					<div class="about-item">
						<div class="ai-text">
							<h4>What we love to do</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
						</div>
					</div>
				 <a href="" class="site-btn sb-gradient mt-5">explore more</a> 
				</div>
			</div>
        </div>
	</section> -->
	<!-- About Section end -->


    <!-- Acts Section -->
	<section class="classes-section spad">
		<div class="container">
			<div class="section-title text-center">
				<h2>Our Services</h2>
				<p>we practice circus to be perfect and  take care of your soul and enjoy life more fully!</p>
			</div>
			<div class="classes-slider owl-carousel">
				<div class="classes-item">
					<div class="ci-img">
						<img src="assets/img/circus/3.jpg" alt="">
					</div>
					<div class="ci-text">
						<h4><a href="classes-details.html">Circus Name</a></h4>
						<p>Generate Lorem Ipsum placeholder text for use in your graphic, print and web layouts, and discover plugins for your favorite writing</p>
					</div>
					<div class="ci-bottom">
						<a href="" class="site-btn sb-gradient">book now</a>
					</div>
                </div>
                <div class="classes-item">
					<div class="ci-img">
						<img src="assets/img/circus/5.jpg" alt="">
					</div>
					<div class="ci-text">
						<h4><a href="classes-details.html">Circus Name</a></h4>
						<p>Generate Lorem Ipsum placeholder text for use in your graphic, print and web layouts, and discover plugins for your favorite writing</p>
					</div>
					<div class="ci-bottom">
						<a href="" class="site-btn sb-gradient">book now</a>
					</div>
                </div>
                <div class="classes-item">
					<div class="ci-img">
						<img src="assets/img/circus/2.jpg" alt="">
					</div>
					<div class="ci-text">
						<h4><a href="classes-details.html">Circus Name</a></h4>
						<p>Generate Lorem Ipsum placeholder text for use in your graphic, print and web layouts, and discover plugins for your favorite writing</p>
					</div>
					<div class="ci-bottom">
						<a href="" class="site-btn sb-gradient">book now</a>
					</div>
				</div>
				<div class="classes-item">
					<div class="ci-img">
						<img src="assets/img/circus/2.jpg" alt="">
					</div>
					<div class="ci-text">
						<h4><a href="classes-details.html">Circus Name</a></h4>
						<p>Generate Lorem Ipsum placeholder text for use in your graphic, print and web layouts, and discover plugins for your favorite writing</p>
					</div>
					<div class="ci-bottom">
						<a href="" class="site-btn sb-gradient">book now</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Classes Section end -->

	<!-- Trainer Section -->
	<section class="trainer-section overflow-hidden spad">
		<div class="container">
			<div class="section-title text-center">
				<h2>Our Circus Trainer</h2>
				<p>Our best teachers will provide the best training to all students</p>
			</div>
			<div class="trainer-slider owl-carousel">
				<div class="ts-item">
					<div class="trainer-item">
						<div class="ti-img">
							<img src="assets/img/trainer/1.jpg" alt="">
						</div>
						<div class="ti-text">
							<h4>Adrian Kennedy</h4>
							<h6>Circus Trainer</h6>
							<p>Adrian Kennedy has 7 years exprience in circus as a traniner</p>
							<div class="ti-social">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-instagram"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-linkedin"></i></a>
							</div>
						</div>
					</div>
                </div>
                <div class="ts-item">
					<div class="trainer-item">
						<div class="ti-img">
							<img src="assets/img/trainer/2.jpg" alt="">
						</div>
						<div class="ti-text">
							<h4>Adrian Kennedy</h4>
							<h6>Circus Trainer</h6>
							<p>Adrian Kennedy has 7 years exprience in circus as a traniner</p>
							<div class="ti-social">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-instagram"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-linkedin"></i></a>
							</div>
						</div>
					</div>
                </div>
                <div class="ts-item">
					<div class="trainer-item">
						<div class="ti-img">
							<img src="assets/img/trainer/3.jpg" alt="">
						</div>
						<div class="ti-text">
							<h4>Adrian Kennedy</h4>
							<h6>Circus Trainer</h6>
							<p>Adrian Kennedy has 7 years exprience in circus as a traniner</p>
							<div class="ti-social">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-instagram"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-linkedin"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Trainer Section end -->

	<!-- Review Section -->
	<section class="review-section spad set-bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 m-auto">
                <div class="section-title text-center text-white">
                    <h2>Testimonials</h2>
                </div>
					<div class="review-slider owl-carousel">
						<div class="review-item">
							<div class="ri-img">
								<img src="assets/img/classes/author/1.jpg" alt="">
							</div>
							<div class="ri-text text-white">
								<p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness.</p>
								<h4>Denise Thomas</h4>
								<h6>Designer</h6>
							</div>
						</div>
						<div class="review-item">
							<div class="ri-img">
								<img src="assets/img/classes/author/2.jpg" alt="">
							</div>
							<div class="ri-text text-white">
								<p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness.</p>
								<h4>Denise Thomas</h4>
								<h6>Designer</h6>
							</div>
						</div>
						<div class="review-item">
							<div class="ri-img">
								<img src="assets/img/classes/author/3.jpg" alt="">
							</div>
							<div class="ri-text text-white">
								<p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness.</p>
								<h4>Denise Thomas</h4>
								<h6>Designer</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Review Section end -->

	<!-- Event Section -->
	<!-- <section class="event-section spad">
		<div class="container">
			<div class="section-title text-center">
				<img src="assets/img/icons/logo-icon.png" alt="">
				<h2>Upcoming Events</h2>
				<p>Practice Yoga to perfect physical beauty, take care of your soul and enjoy life more fully!</p>
			</div>
			<div class="row">
				<div class="col-xl-6">
					<div class="event-video">
						<img src="assets/img/video.jpg" alt="">
						<a href="https://www.youtube.com/watch?v=vgv-hzTl5FA" class="video-popup"><img src="img/icons/play.png" alt=""></a>
					</div>
				</div>
				<div class="col-xl-6">
					<div class="event-item">
						<div class="ei-img">
							<img src="assets/img/event/1.jpg" alt="">
						</div>
						<div class="ei-text">
							<h4><a href="#">Lole White Yoga Tour</a></h4>
							<ul>
								<li><i class="material-icons">person</i>Kelly Alexander</li>
								<li><i class="material-icons">event_available</i>15 January, 2019</li>
								<li><i class="material-icons">map</i>184 Main Collins Street</li>
							</ul>
						</div>
					</div>
					<div class="event-item">
						<div class="ei-img">
							<img src="assets/img/event/2.jpg" alt="">
						</div>
						<div class="ei-text">
							<h4>Free Yoga Madrid</h4>
							<ul>
								<li><i class="material-icons">person</i>Kelly Alexander</li>
								<li><i class="material-icons">event_available</i>15 January, 2019</li>
								<li><i class="material-icons">map</i>184 Main Collins Street</li>
							</ul>
						</div>
					</div>
					<div class="event-item">
						<div class="ei-img">
							<img src="assets/img/event/3.jpg" alt="">
						</div>
						<div class="ei-text">
							<h4>One Love Dallas</h4>
							<ul>
								<li><i class="material-icons">person</i>Kelly Alexander</li>
								<li><i class="material-icons">event_available</i>15 January, 2019</li>
								<li><i class="material-icons">map</i>184 Main Collins Street</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> -->
	<!-- Event Section end -->

    <!-- Include price section -->

    <?php include_once 'partials/footer.php'?>