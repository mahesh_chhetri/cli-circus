<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="./assets/css/bootstrap.css">
    <link rel="shortcut icon" href="./assets/img/icons\favicon.ico" />
    <link rel="stylesheet" href="./assets/css/custom.css">
    
    <title>Make Your Web</title>
  </head>
  <body>
      
  <?php include('partials/header.php') ?>

<div class="my-5">

  <div class="col-md-12 px-0 p-5 border-bottom news">
    <div class="row mx-0">
      <div class="container py-2 px-4">
          <blockquote class="blockquote">
          <h4 class="mb-0 theam-color">Sales <small class="float-right">24th july, 2020</small></h4>
          <footer class="blockquote-footer my-2"> You can now order from <a href="http://makeyourweb.co.uk/quote/">the contact us page</a>. Or even get a custom quote. </footer>
        </blockquote>
      </div>
    </div>
  </div>

   <div class="col-md-12 px-0 p-5 border-bottom news">
      <div class="row mx-0">
        <div class="container py-2 px-4">
            <blockquote class="blockquote">
            <h4 class="mb-0 theam-color">Running our business <small class="float-right">22th july, 2020</small></h4>
            <footer class="blockquote-footer my-2"> This is the starting of <a href="http://makeyourweb.co.uk/">Make Your Web</a> and the start of this wonderful business. </footer>
          </blockquote>
        </div>
      </div>
    </div>
    
    <div class="col-md-12 px-0 p-5 border-bottom news">
      <div class="row mx-0">
        <div class="container py-2 px-4">
            <blockquote class="blockquote">
            <h4 class="mb-0 theam-color">The starting<small class="float-right">13 july, 2020</small></h4>
            <footer class="blockquote-footer my-2">Co-Founder Graham, thought we should make people websites. As of this idea we had started this servive.</footer>
          </blockquote>
        </div>
      </div>
    </div>
</div> 

      <!-- footer -->
      <?php include('partials/footer.php') ?>
  <!-- end footer -->
  </body>
</html>