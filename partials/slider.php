<!-- Hero Section -->
<section class="hero-section">
		<div class="hero-social-warp">
			<div class="hero-social">
				<a href="#"><i class="fa fa-facebook"></i></a>
				<a href="#"><i class="fa fa-instagram"></i></a>
				<a href="#"><i class="fa fa-twitter"></i></a>
				<a href="#"><i class="fa fa-linkedin"></i></a>
			</div>
		</div>
		<div class="arrow-buttom">
			<img src="assets/img/icons/arrows-buttom.png" alt="">
		</div>
		<div class="hero-slider owl-carousel">
			<div class="hs-item">
				<div class="hs-style-2">
					<div class="container-fluid ">
						<div class="row">
							<div class="col-lg-6  d-none d-lg-flex align-items-xl-end align-items-lg-center">
								<div class="hs-img">
								<img src="assets/img/slider/1.jpg" alt="">
								</div>
							</div>
							<div class="col-lg-6 d-flex align-items-center">
								<div class="hs-text-warp">
									<div class="hs-text">
									<h2>Circus Name</h2>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit </p>
										<div class="site-btn sb-white">Book Now</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="hs-item">
				<div class="hs-style-2">
					<div class="container-fluid ">
						<div class="row">
							<div class="col-lg-6  d-none d-lg-flex align-items-xl-end align-items-lg-center">
								<div class="hs-img">
								<img src="assets/img/slider/2.jpg" alt="">
								</div>
							</div>
							<div class="col-lg-6 d-flex align-items-center">
								<div class="hs-text-warp">
									<div class="hs-text">
									<h2>Circus Name</h2>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit </p>
										<div class="site-btn sb-white">Book Now</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="hs-item">
				<div class="hs-style-2">
					<div class="container-fluid ">
						<div class="row">
							<div class="col-lg-6  d-none d-lg-flex align-items-xl-end align-items-lg-center">
								<div class="hs-img">
								<img src="assets/img/slider/3.jpg" alt="">
								</div>
							</div>
							<div class="col-lg-6 d-flex align-items-center">
								<div class="hs-text-warp">
									<div class="hs-text">
										<h2>Circus Name</h2>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit </p>
										<div class="site-btn sb-white">Book Now</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Hero Section end -->