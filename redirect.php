<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="./assets/css/bootstrap.css">
    <link rel="shortcut icon" href="./assets\img\icons\favicon.ico" />
    <link rel="stylesheet" href="./assets/css/custom.css">
    
    <title>Make Your Web</title>
  </head>


    <script>
        function startTimer(duration, display) {
            var timer = duration, seconds;
            var end =setInterval(function () {
                seconds = parseInt(timer % 60, 10);
                empty = ""

                seconds = seconds < 10 ? "0" + seconds:empty;

                display.textContent = seconds;

                if (--timer < 0) {
                    window.location = "http://www.makeyourweb.co.uk";
                    clearInterval(end);
                }
            }, 1000);
        }

        window.onload = function () {
            var fiveMinutes = 5,
                display = document.querySelector('#time');
            startTimer(fiveMinutes, display);
        };
    </script>
<?php include('partials/header.php'); ?>

    </head>
    <body>

        <style>

            h1{
                text-align: center;
                margin-top: 10%;
            }

            .timee{
                text-shadow: blue;
            }

            .redirect{
                text-align: center;
                font-size: 300%;
                color: red;
            }

        </style>

    <div>
            <h1>Redirecting to homepage in <div class="timee"></div><span id="time" >5</span></div><div class="redirect"> 

                <a class="btn btn-outline-warning" href="http://makeyourweb.co.uk">Redirect now</a>
            </div>

            </h1>
    </div>


    <form id="form1" runat="server">

    </form>


          <!-- footer -->
          <?php include('partials/footer.php');?>
        <!-- end footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script> -->
    
    <script src="./assets/js/bootstrap.min.js"></script>